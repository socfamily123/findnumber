package com.example.findnumber;


import androidx.lifecycle.MutableLiveData;

public final class Storage {
    private final MutableLiveData<Boolean> soundON = new MutableLiveData<>();
    private final MutableLiveData<Boolean> nightMode = new MutableLiveData<>();
    private int currentNumber = 1;
    private int difficult = 1;

    public int getDifficult() {
        return difficult;
    }

    public void setDifficult(int difficult) {
        this.difficult = difficult;
    }

    public int getCurrentNumber() {
        return currentNumber;
    }

    public void setCurrentNumber(int currentNumber) {
        this.currentNumber = currentNumber;
    }

    public MutableLiveData<Boolean> isNightMode() {
        return nightMode;
    }

    public MutableLiveData<Boolean> isSoundON() {
        return soundON;
    }

    public void setNightMode(Boolean nightMode) {
        this.nightMode.setValue(nightMode);
    }

    public void setSoundON(Boolean soundON) {
        this.soundON.setValue(soundON);
    }
}
