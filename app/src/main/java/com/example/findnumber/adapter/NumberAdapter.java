package com.example.findnumber.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.findnumber.App;
import com.example.findnumber.R;
import com.example.findnumber.entity.Number;

import java.util.List;
import java.util.Random;

import static com.example.findnumber.view.fragment.M001MenuGameFrg.DIFFICULT_CHALLENGE;
import static com.example.findnumber.view.fragment.M001MenuGameFrg.DIFFICULT_HARD;
import static com.example.findnumber.view.fragment.M001MenuGameFrg.DIFFICULT_SIMPLE;

public class NumberAdapter extends RecyclerView.Adapter<NumberAdapter.NumberHolder> {
    private final List<Number> listNumber;
    private final Context mContext;
    private NumberAdapter.CallBackNumber callBackNumber;

    public NumberAdapter(List<Number> listNumber, Context mContext) {
        this.listNumber = listNumber;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public NumberHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_number, parent, false);
        return new NumberHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NumberHolder holder, int position) {
        Number number = listNumber.get(position);
        holder.number = number;
        holder.tvNumber.setText(number.getNumber() + "");
        randomTextView(holder.tvNumber);
    }

    private void randomTextView(TextView tvNumber) {
        int[] gravities = new int[8];
        gravities[0] = Gravity.BOTTOM | Gravity.CENTER;
        gravities[1] = Gravity.TOP | Gravity.CENTER;
        gravities[2] = Gravity.LEFT | Gravity.TOP;
        gravities[3] = Gravity.LEFT | Gravity.BOTTOM;
        gravities[4] = Gravity.LEFT | Gravity.CENTER;
        gravities[5] = Gravity.RIGHT | Gravity.TOP;
        gravities[6] = Gravity.RIGHT | Gravity.BOTTOM;
        gravities[7] = Gravity.RIGHT | Gravity.CENTER;

        Random rd = new Random();

        //random COLOR
        tvNumber.setTextColor(Color.rgb(rd.nextInt(200) + 28,
                rd.nextInt(200) + 28,
                rd.nextInt(200) + 28));

        //random gravity && rotation
        int randomIndex = rd.nextInt(gravities.length);
        int randomGravity = gravities[randomIndex];
        tvNumber.setGravity(randomGravity);

        int padding = rd.nextInt(5) + 5;
        int padding2 = rd.nextInt(5) + 5;
        int padding3 = rd.nextInt(5) + 5;
        int padding4 = rd.nextInt(5) + 5;
        tvNumber.setPadding(padding, padding2, padding3, padding4);

        //theo difficult
        int difficult = App.getInstance().getStorage().getDifficult();
        switch (difficult) {
            case DIFFICULT_SIMPLE:
                break;
            case DIFFICULT_HARD:
                if (randomIndex == 2 || randomIndex == 3 || randomIndex == 4 || randomIndex == 5 || randomIndex == 1) {
                    tvNumber.setRotation(rd.nextInt(360));
                    tvNumber.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                }
                break;
            case DIFFICULT_CHALLENGE:
                if (randomIndex == 2 || randomIndex == 3 || randomIndex == 4 || randomIndex == 5 || randomIndex == 1) {
                    tvNumber.setRotation(rd.nextInt(360));
                    tvNumber.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    tvNumber.setTextSize(rd.nextInt(8) + 14);
                }
                break;
        }


    }

    @Override
    public int getItemCount() {
        return listNumber.size();
    }

    public void setCallBackNumber(CallBackNumber callBackNumber) {
        this.callBackNumber = callBackNumber;
    }

    public interface CallBackNumber {
        void backData(int number, TextView textView);
    }

    public class NumberHolder extends RecyclerView.ViewHolder {
        LinearLayout lnNumber;
        Number number;
        TextView tvNumber;


        public NumberHolder(@NonNull View itemView) {
            super(itemView);
            lnNumber = itemView.findViewById(R.id.ln_number);
            tvNumber = itemView.findViewById(R.id.tv_number);

            tvNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callBackNumber.backData(number.getNumber(), tvNumber);

                }
            });
        }
    }
}
