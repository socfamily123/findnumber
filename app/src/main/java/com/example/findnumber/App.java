package com.example.findnumber;

import android.app.Application;

public final class App extends Application {
    private static App instance;
    private Storage storage;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        storage = new Storage();
        instance = this;

    }


    public Storage getStorage() {
        return storage;
    }
}
