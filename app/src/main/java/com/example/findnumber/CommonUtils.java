package com.example.findnumber;


import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class CommonUtils {
    private static final String PREF_FILE = "pref_file";
    private static CommonUtils instance;

    private CommonUtils() {
    }

    public static CommonUtils getInstance() {
        if (instance == null) {
            instance = new CommonUtils();
        }
        return instance;
    }

    public String getTextFile(InputStream input) {
        try {
            BufferedReader b = new BufferedReader(new InputStreamReader(input, StandardCharsets.UTF_8));
            String line;
            StringBuilder out = new StringBuilder();
            while ((line = b.readLine()) != null) {
                out.append(line);
            }
            b.close();
            input.close();
            return out.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public void clearPref(String key) {
        SharedPreferences pref = App.getInstance().getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        pref.edit().remove(key).apply();
    }

    public String getPref(String key, boolean isDelete) {
        SharedPreferences pref = App.getInstance().getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        String vl = pref.getString(key, null);
        if (vl != null && isDelete) {
            pref.edit().remove(key).apply();
        }
        return vl;
    }

    public String getPref(String key) {
        return getPref(key, false);
    }

    public void savePref(String key, String value) {
        SharedPreferences pref = App.getInstance().getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        pref.edit().putString(key, value).apply();
    }

    public boolean isEmail(String email) {
        Pattern pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.find();
    }

    public boolean isPhone(String phone) {
        return phone.matches("^(09|03|07|08|05)\\d{8}$");
    }

    public void saveToData(String fileName, String text) {
        String fullPath = Environment.getDataDirectory().getPath() + "/data/" + App.getInstance().getPackageName() + "/" + fileName;
        writeTextFile(fullPath, text);
    }

    private void writeTextFile(String fullPath, String text) {
        try {
            FileOutputStream out = new FileOutputStream(new File(fullPath));
            out.write(text.getBytes());
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getFromData(String fileName) {
        String fullPath = Environment.getDataDirectory().getPath() + "/data/" + App.getInstance().getPackageName() + "/" + fileName;
        return readtextFile(fullPath);
    }

    private String readtextFile(String fullPath) {
        try {
            FileInputStream in = new FileInputStream(new File(fullPath));
            byte[] buff = new byte[1024];
            int len = in.read(buff);
            StringBuilder txt = new StringBuilder();
            while (len > 0) {
                txt.append(new String(buff, 0, len));
                len = in.read(buff);
            }
            in.close();
            return txt.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void saveToExternal(String fileName, String text) {
        String fullPath = App.getInstance().getExternalFilesDir(null).getPath() + "/" + fileName;
        writeTextFile(fullPath, text);
    }

    public String getFromExternal(String fileName) {
        String fullPath = App.getInstance().getExternalFilesDir(null).getPath() + "/" + fileName;
        return readtextFile(fullPath);
    }

    public boolean isInternetAvailable() {
        ConnectivityManager
                cm = (ConnectivityManager) App.getInstance().getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnectedOrConnecting();
    }

    public void forceHideKeyBoard(View focusView) {
        if (focusView != null) {
            InputMethodManager imm = (InputMethodManager) App.getInstance()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0);
        }
    }
}
