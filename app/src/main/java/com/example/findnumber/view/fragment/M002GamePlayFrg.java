package com.example.findnumber.view.fragment;

import android.media.MediaPlayer;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.findnumber.CommonUtils;
import com.example.findnumber.MTask;
import com.example.findnumber.R;
import com.example.findnumber.adapter.NumberAdapter;
import com.example.findnumber.entity.Number;
import com.example.findnumber.view.base.BaseFragment;
import com.example.findnumber.view.callback.OnActionCallBack;
import com.example.findnumber.view.dialog.DialogSetting;
import com.example.findnumber.view.model.M002GamePlayModel;

import static com.example.findnumber.view.dialog.DialogSetting.KEY_CHECK_NIGHT;
import static com.example.findnumber.view.dialog.DialogSetting.KEY_CHECK_SOUND;


public class M002GamePlayFrg extends BaseFragment<M002GamePlayModel> implements MTask.OnMTaskCallBack, OnActionCallBack {
    public static final String TAG = M002GamePlayFrg.class.getName();
    private static final String KEY_UPDATE_TIME_SEC = "KEY_UPDATE_TIME";
    private static final int NUMBER = 100;
    NumberAdapter numberAdapter;
    private RecyclerView rvNumber;
    private TextView tvCurrentNumber, tvTime;
    private LinearLayout lnNumber;
    private MTask mTask;
    private Animation animation;
    private MediaPlayer mediaClick, mediaSuccess;
    private DialogSetting dialogSetting;

    @Override
    public void backToPreviousScreen() {
    }

    @Override
    protected Class<M002GamePlayModel> getClassViewModel() {
        return M002GamePlayModel.class;
    }

    @Override
    protected void initViews() {
        rvNumber = findViewById(R.id.rv_number);
        rvNumber.setLayoutManager(new GridLayoutManager(mContext, 10));

        tvCurrentNumber = findViewById(R.id.tv_current_number);
        tvTime = findViewById(R.id.tv_time);
        findViewById(R.id.iv_find).setOnClickListener(this);
        findViewById(R.id.iv_setting).setOnClickListener(this);
        findViewById(R.id.iv_back).setOnClickListener(this);
        mediaClick = MediaPlayer.create(mContext, R.raw.click_shot);

        lnNumber = findViewById(R.id.ln_number);
        dialogSetting = new DialogSetting(mContext);
        dialogSetting.setCallBack(this);

        getStorage().setNightMode(Boolean.valueOf(CommonUtils.getInstance().getPref(KEY_CHECK_NIGHT)));
        getStorage().setSoundON(Boolean.valueOf(CommonUtils.getInstance().getPref(KEY_CHECK_SOUND)));

        getStorage().isNightMode().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    lnNumber.setBackgroundResource(R.color.black);
                } else lnNumber.setBackgroundResource(R.drawable.im_bg2);
            }
        });
        initData();
    }

    @Override
    public void onClick(View view) {
        getStorage().isSoundON().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    mediaClick.start();
                }
            }
        });
        if (view.getId() == R.id.iv_find) {
            animation = AnimationUtils.loadAnimation(mContext, R.anim.anim_fadein);
            for (int i = 0; i < mModel.getListNumber().size(); i++) {
                if (mModel.getListNumber().get(i).getNumber() == getStorage().getCurrentNumber()) {
                    rvNumber.findViewHolderForLayoutPosition(i).itemView.startAnimation(animation);
                    return;
                }
            }
        } else if (view.getId() == R.id.iv_setting) {
            dialogSetting.show();
        } else if (view.getId() == R.id.iv_back) {
            getStorage().setCurrentNumber(1);
            mCallBack.showFrg(TAG, M001MenuGameFrg.TAG);
        }
    }

    private void endGame() {
        if (getStorage().getCurrentNumber() == NUMBER + 1) {
            mTask.stop();
            showAlert("Congratulations to you\nYour completion time is " + tvTime.getText(), "Play again?", "Chán rồi", "OK", new OnAlertCallBack() {
                @Override
                public void callBack(int key, Object data) {
                    if (key == AlertDialog.BUTTON_POSITIVE) {
                        mCallBack.closeApp();
                    } else if (key == AlertDialog.BUTTON_NEGATIVE) {
                        getStorage().setCurrentNumber(1);
                        loadUI();
                    }
                }
            });
        }
    }

    private void initData() {
        for (int i = 1; i <= NUMBER; i++) {
            Number number = new Number(i);
            if ((mModel.getListNumber().indexOf(number) <= 0)) {
                mModel.addNumber(number);
            }
        }
        mModel.shufflerNumber();
        loadUI();
    }

    private void loadUI() {
        tvCurrentNumber.setText(getStorage().getCurrentNumber() + "");
        mTask = new MTask(KEY_UPDATE_TIME_SEC, this);
        mTask.start(0);
        numberAdapter = new NumberAdapter(mModel.getListNumber(), mContext);
        numberAdapter.setCallBackNumber(new NumberAdapter.CallBackNumber() {
            @Override
            public void backData(int number, TextView textView) {
                if (number == getStorage().getCurrentNumber()) {
                    textView.setText("X");
                    getStorage().setCurrentNumber(number + 1);
                    tvCurrentNumber.setText(getStorage().getCurrentNumber() + "");
//                    getStorage().isSoundON().observe(M002GamePlayFrg.this,
//                            new Observer<Boolean>() {
//                                @Override
//                                public void onChanged(Boolean aBoolean) {
//                                    if (aBoolean) {
//                                        mediaSuccess = MediaPlayer.create(mContext, R.raw.success);
//                                        mediaSuccess.start();
//                                    }
//                                }
//                            });
                    if (Boolean.parseBoolean(CommonUtils.getInstance().getPref(KEY_CHECK_SOUND))) {
                        mediaSuccess = MediaPlayer.create(mContext, R.raw.success);
                        mediaSuccess.start();
                    }
                    endGame();
                }
            }
        });
        rvNumber.setAdapter(numberAdapter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m002_play_game;
    }

    @Override
    public Object execTask(String key, MTask task, Object data) {
        if (key.equals(KEY_UPDATE_TIME_SEC)) {
            int count = (int) data;
            try {
                for (int i = count; i <= Integer.MAX_VALUE; i++) {
                    Thread.sleep(1000);
                    task.requestUpdate(i + "");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public void completeTask(String key, Object value) {
    }

    @Override
    public void updateUI(String key, Object data) {
        int seconds = Integer.parseInt((String) data);
        int minutes = (seconds % 3600) / 60;
        int hours = seconds / 3600;
        String minutes_output = minutes + "";
        String hours_output = hours + "";
        String seconds_output = (seconds % 3600) % 60 + "";
        if ((seconds_output).length() == 1) {
            seconds_output = "0" + (seconds % 3600) % 60;
        }
        if (minutes_output.length() == 1) {
            minutes_output = "0" + (seconds % 3600) / 60;
        }
        if (hours_output.length() == 1) {
            hours_output = seconds / 3600 + "0";
        }
        if (hours == 0) {
            tvTime.setText(minutes_output + ":" + seconds_output);
        } else tvTime.setText(hours_output + ":" + minutes_output + ":" + seconds_output);
    }


    @Override
    public void callBack(String key, Object data) {
        if (key.equals(KEY_CHECK_NIGHT)) {
            getStorage().setNightMode((boolean) data);
        } else if (key.equals(KEY_CHECK_SOUND)) {
            getStorage().setSoundON((boolean) data);
        }
    }
}
