package com.example.findnumber.view.fragment;

import android.os.Handler;

import com.example.findnumber.R;
import com.example.findnumber.view.base.BaseFragment;
import com.example.findnumber.view.model.M000SplashViewModel;

public class M000SplashFrg extends BaseFragment<M000SplashViewModel> {
    public static final String TAG = M000SplashFrg.class.getName();

    @Override
    public void backToPreviousScreen() {

    }

    @Override
    protected Class<M000SplashViewModel> getClassViewModel() {
        return M000SplashViewModel.class;
    }

    @Override
    protected void initViews() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mCallBack.showFrg(TAG,M001MenuGameFrg.TAG);
            }
        },2000);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m000_splash;
    }
}
