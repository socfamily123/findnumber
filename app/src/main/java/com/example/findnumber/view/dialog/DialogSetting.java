package com.example.findnumber.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

import androidx.annotation.NonNull;

import com.example.findnumber.CommonUtils;
import com.example.findnumber.R;
import com.example.findnumber.view.callback.OnActionCallBack;

public class DialogSetting extends Dialog implements View.OnClickListener {
    public static final String KEY_CHECK_NIGHT = "KEY_CHECK_NIGHT";
    public static final String KEY_CHECK_SOUND = "KEY_CHECK_SOUND";
    private final Context mContext;
    private CheckBox cbNight, cbSound;
    private OnActionCallBack callBack;

    public DialogSetting(@NonNull Context context) {
        super(context);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        setContentView(R.layout.view_dialog_setting);
        mContext = context;
        initViews();


    }

    public void setCallBack(OnActionCallBack callBack) {
        this.callBack = callBack;
    }

    private void initViews() {
        findViewById(R.id.bt_ok).setOnClickListener(this);

        cbNight = findViewById(R.id.cb_night);
        cbSound = findViewById(R.id.cb_sound);
        cbNight.setOnClickListener(this);
        cbSound.setOnClickListener(this);

        cbNight.setChecked(Boolean.parseBoolean(CommonUtils.getInstance().getPref(KEY_CHECK_NIGHT)));
        cbSound.setChecked(Boolean.parseBoolean(CommonUtils.getInstance().getPref(KEY_CHECK_SOUND)));


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callBack.callBack(KEY_CHECK_NIGHT, cbNight.isChecked());
        callBack.callBack(KEY_CHECK_SOUND, cbSound.isChecked());
    }

    @Override
    public void onClick(View view) {
        callBack.callBack(KEY_CHECK_NIGHT, cbNight.isChecked());
        callBack.callBack(KEY_CHECK_SOUND, cbSound.isChecked());
        CommonUtils.getInstance().savePref(KEY_CHECK_NIGHT, cbNight.isChecked() + "");
        CommonUtils.getInstance().savePref(KEY_CHECK_SOUND, cbSound.isChecked() + "");
        if (view.getId() == R.id.bt_ok) dismiss();
    }


}
