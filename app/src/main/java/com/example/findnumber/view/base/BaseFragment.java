package com.example.findnumber.view.base;


import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.findnumber.App;
import com.example.findnumber.Storage;
import com.example.findnumber.view.callback.OnHomeCallBack;

public abstract class BaseFragment<T extends ViewModel> extends Fragment implements View.OnClickListener {
    protected Context mContext;
    protected View mRootView;
    protected OnHomeCallBack mCallBack;
    protected Object mData;
    protected T mModel;
    protected String tagSource;

    public String getTagSource() {
        return tagSource;
    }

    public void setTagSource(String tagSource) {
        this.tagSource = tagSource;
        defineBackKey();
    }

    //define tag when back press
    public void defineBackKey() {
        //do nothing
    }

    public abstract void backToPreviousScreen();

    public final void setCallBack(OnHomeCallBack mCallBack) {
        this.mCallBack = mCallBack;
    }

    @Override
    public final void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public final View onCreateView(@NonNull LayoutInflater inflater,
                                   @Nullable ViewGroup container,
                                   @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(getLayoutId(), container, false);
        mModel = new ViewModelProvider(this).get(getClassViewModel());

        initViews();
        return mRootView;
    }

    protected abstract Class<T> getClassViewModel();

    protected abstract void initViews();

    protected abstract int getLayoutId();

    public final <K extends View> K findViewById(int id, View.OnClickListener event) {
        K v = findViewById(id);
        if (v != null && event != null) {
            v.setOnClickListener(event);
        }
        return v;
    }

    public final <L extends View> L findViewById(int id) {
        return mRootView.findViewById(id);
    }

    @Override
    public void onClick(View view) {
        // do nothing
    }

    public void setData(Object data) {
        mData = data;
    }

    protected void showAlert(String title, String msg,
                             String txtBt1, String txtBt2,
                             final OnAlertCallBack callBack) {

        AlertDialog alert = new AlertDialog.Builder(mContext).create();
        alert.setCanceledOnTouchOutside(false);
        alert.setCancelable(false);

        alert.setTitle(title);
        alert.setMessage(msg);
        if (txtBt1 != null) {
            alert.setButton(AlertDialog.BUTTON_POSITIVE,
                    txtBt1, (dialogInterface, i) -> {
                        callBack.callBack(AlertDialog.BUTTON_POSITIVE, null);
                    });
        }
        if (txtBt2 != null) {
            alert.setButton(AlertDialog.BUTTON_NEGATIVE,
                    txtBt2, (dialogInterface, i) -> {
                        callBack.callBack(AlertDialog.BUTTON_NEGATIVE, null);
                    });
        }
        alert.show();
    }

    protected Storage getStorage() {
        return App.getInstance().getStorage();
    }

    public interface OnAlertCallBack {
        void callBack(int key, Object data);
    }
}