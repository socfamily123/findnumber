package com.example.findnumber.view.activity;

import com.example.findnumber.R;
import com.example.findnumber.view.base.BaseAct;
import com.example.findnumber.view.fragment.M000SplashFrg;
import com.example.findnumber.view.model.MainViewModel;

public class HomeAct extends BaseAct<MainViewModel> {


    private static final String TAG = HomeAct.class.getName();

    @Override
    protected Class<MainViewModel> getClassViewModel() {
        return MainViewModel.class;
    }

    @Override
    protected void initViews() {
        showFrg(TAG, M000SplashFrg.TAG);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }
}