package com.example.findnumber.view.model;

import androidx.lifecycle.ViewModel;

import com.example.findnumber.entity.Number;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class M002GamePlayModel extends ViewModel {
    private List<Number> listNumber = new ArrayList<>();

    public List<Number> getListNumber() {
        return listNumber;
    }

    public void setListNumber(List<Number> listNumber) {
        this.listNumber = listNumber;
    }

    public void addNumber(Number number) {
        listNumber.add(number);
    }

    public void shufflerNumber() {
        Collections.shuffle(listNumber);
    }


}
