package com.example.findnumber.view.fragment;

import android.media.MediaPlayer;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;

import com.example.findnumber.CommonUtils;
import com.example.findnumber.R;
import com.example.findnumber.view.base.BaseFragment;
import com.example.findnumber.view.callback.OnActionCallBack;
import com.example.findnumber.view.dialog.DialogSetting;
import com.example.findnumber.view.model.M001MenuGameModelView;

import static com.example.findnumber.view.dialog.DialogSetting.KEY_CHECK_NIGHT;
import static com.example.findnumber.view.dialog.DialogSetting.KEY_CHECK_SOUND;

public class M001MenuGameFrg extends BaseFragment<M001MenuGameModelView> implements OnActionCallBack {
    public static final String TAG = M001MenuGameFrg.class.getName();
    public static final int DIFFICULT_SIMPLE = 1;
    public static final int DIFFICULT_HARD = 2;
    public static final int DIFFICULT_CHALLENGE = 3;
    private LinearLayout lnMain;
    private DialogSetting dialogSetting;
    private MediaPlayer mediaClick;

    @Override
    public void backToPreviousScreen() {
        mCallBack.closeApp();

    }

    @Override
    protected Class<M001MenuGameModelView> getClassViewModel() {
        return M001MenuGameModelView.class;
    }

    @Override
    protected void initViews() {
        findViewById(R.id.ln_simple).setOnClickListener(this);
        findViewById(R.id.ln_hard).setOnClickListener(this);
        findViewById(R.id.ln_challenge).setOnClickListener(this);
        findViewById(R.id.iv_back).setOnClickListener(this);
        findViewById(R.id.iv_setting).setOnClickListener(this);
        lnMain = findViewById(R.id.ln_main);
        dialogSetting = new DialogSetting(mContext);
        dialogSetting.setCallBack(this);

        mediaClick = MediaPlayer.create(mContext, R.raw.click_shot);

        getStorage().setNightMode(Boolean.valueOf(CommonUtils.getInstance().getPref(KEY_CHECK_NIGHT)));
        getStorage().setSoundON(Boolean.valueOf(CommonUtils.getInstance().getPref(KEY_CHECK_SOUND)));

        getStorage().isNightMode().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    lnMain.setBackgroundResource(R.color.night);
                } else lnMain.setBackgroundResource(R.color.bg);
            }
        });
    }

    @Override
    public void onClick(View view) {
        getStorage().isSoundON().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    mediaClick.start();
                }
            }
        });
        switch (view.getId()) {
            case R.id.iv_back:
                showAlert("Exit", "Close this app?", "Ấn nhầm", "OK", new OnAlertCallBack() {
                    @Override
                    public void callBack(int key, Object data) {
                        if (key == AlertDialog.BUTTON_NEGATIVE) {
                            mCallBack.closeApp();
                        }
                    }
                });
                break;
            case R.id.iv_setting:
                dialogSetting.show();
                break;
            case R.id.ln_simple:
                getStorage().setDifficult(DIFFICULT_SIMPLE);
                showNextFrg();
                break;
            case R.id.ln_hard:
                getStorage().setDifficult(DIFFICULT_HARD);
                showNextFrg();
                break;
            case R.id.ln_challenge:
                getStorage().setDifficult(DIFFICULT_CHALLENGE);
                showNextFrg();
                break;
        }

    }

    private void showNextFrg() {
        mCallBack.showFrg(TAG, M002GamePlayFrg.TAG);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m001_menu_game;
    }

    @Override
    public void callBack(String key, Object data) {
        if (key.equals(KEY_CHECK_NIGHT)) {
            getStorage().setNightMode((boolean) data);
        } else if (key.equals(KEY_CHECK_SOUND)) {
            getStorage().setSoundON((boolean) data);
        }
    }
}
