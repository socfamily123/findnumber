package com.example.findnumber.view.callback;

public interface OnActionCallBack  {
    void callBack(String key, Object data);
}
