package com.example.findnumber.view.callback;

public interface OnHomeCallBack {
    void showFrg(String backTag, String tag);
    void showFrg(String backTag, Object data, String tag);
    void closeApp();
}
